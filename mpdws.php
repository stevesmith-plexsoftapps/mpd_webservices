<?php
include ("dbutils.php");
include_once("mpdws_common_routines.php");
include_once("mpdws_users.php");
include_once("mpdws_orders.php");
include_once("mpdws_rates.php");

	// instanciate the db utilities class
	$dbUtils = new dbUtils();
	
	// open the db
	$dbUtils->openDB("","","","");
	
	error_log(print_r("***********************", TRUE));

	$input = file_get_contents('php://input');
	$_POST = json_decode($input, true);
	error_log(print_r("input=" . $input, TRUE));

	// see if an entity was passed
	$entity = get_GET_or_POST('entity');
	error_log(print_r("entity=" . $entity, TRUE)); 

	// $entity = "";
	// if(isset($_GET['entity'])) {
	// 	$entity = $_GET['entity']; //no default
	// }

	// see if an action was passed
	$action = get_GET_or_POST('action');
	error_log(print_r("action=" . $action, TRUE)); 
	// $action = "";
	// if(isset($_GET['action'])) {
	// 	$action = $_GET['action']; //no default
	// }

	$json = "";  // "username=me";  // "Got entity " . $entity . " and action " . $action;

	if ($entity != "")
	{
		switch (strtoupper($entity))
		{
			case "USER":
				$json = process_user_action($action, $dbUtils);
				break;

			case "ORDER":
				$json = process_order_action($action, $dbUtils);
				break;

			case "RATE":
				$json = process_rate_action($action, $dbUtils);
				break;

			default: 
				$json = "Unknown entity " . $entity;
				break;
		}
	}
	else 
	{
		$json = "No entity specified";
	}

	if (!is_array($json))
	{
		$json = array($json);
	}

	error_log(print_r("Done", TRUE)); 
	error_log(print_r($json, TRUE)); 

	//header('content-type: application/json');
	echo json_encode($json);
	
	$dbUtils->closeDB();


?>