<?php 


	//*************************************************************************************
	//* CRUD routines for users
	//*************************************************************************************	
    function process_user_action($action, $dbUtils)
    {
        $result = "";

       	// see if a username was passed
        $userid = get_GET_or_POST('userid');
        // if(isset($_GET['userid'])) {
        //     $userid = $_GET['userid']; //no default
        // }
        error_log(print_r("userid=" . $userid, TRUE)); 

        if ($userid != "")
        {
            // process the action
            // redirect to the appropriate function 
            switch (strtoupper($action))
            {
                case "CREATE":
                    $result = create_user($userid,$dbUtils);
                    break;

                case "READ":
                    $result = read_user($userid,$dbUtils);
                    break;

                case "UPDATE":
                    $result = update_user($userid,$dbUtils);
                    break;

                case "DELETE":
                    $result = delete_user($userid,$dbUtils);
                    break;

                default: 
                    $result = "Unknow action " . $action;
                    break;

            }
            
        }
        else 
        {
            $result = "User id not provided ";
        }

        if (!is_array($result) > 0){
            $result = array($result);
        }

        return $result;
    }
    
    function create_user($userid, $dbUtils)
	{

        return "Create not yet implemented";
	}


	function read_user($userid,$dbUtils)
	{

		$msg = "";
		
		// read  the users table
		$sql = "Select * From vwmpd_users";
		
		// if a user id specified, then restrict it by that id
		if($userid <> "") {
			$sql = $sql . " Where username='" . $userid . "'";
		}
        
        error_log(print_r("user sql=" . $sql, TRUE)); 

		$rs = $dbUtils->select($sql);

		while ($row = mysqli_fetch_assoc($rs))
		{
			$msg[] = array("id" =>$row['id'],
										"username" =>$row['username'],
										"accountnumber" =>$row['accountnumber'],
										"firstname" =>$row['firstname'], 
										"lastname" =>$row['lastname'],
										"address" =>$row['address'],
										"city" =>$row['city'],
										"state" =>$row['state'],
										"zipcode" =>$row['zipcode'],
										"primaryphone" =>$row['primaryphone'],
										"email" =>$row['email']
										);
		}
		
		if (is_array($msg) > 0){
			$json = $msg;
		}
		else {
			$json = array("No users found from here...");
		}

		return $json;
		
    }
    
    function update_user($userid, $dbUtils)
    {

        return "Update not yet implemented";
    }


    function delete_user($userid, $dbUtils)
    {

        return "Delete not yet implemented";
    }



	
?>
