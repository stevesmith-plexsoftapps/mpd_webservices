<?php
// Common Routines


function get_GET_or_POST($var)
{
		$value = "";
		
		// Check for a variable, through GET or POST.
		if ( isset($_GET[$var]) ) {// Accessed through URL
			  $value = $_GET[$var];
		} 
		else
		{
				if ( isset($_POST[$var]) ) { //Form has been submitted.
					  $value = $_POST[$var];
				} 
		}
	
		return $value;
}

?>
