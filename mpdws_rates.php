<?php 


	//*************************************************************************************
	//* CRUD routines for rates
	//*************************************************************************************	
    function process_rate_action($action, $dbUtils)
    {
        $result = "";

        // see if a username was passed
        $rateid = get_GET_or_POST('rateid');
        error_log(print_r("rateid=" . $rateid, TRUE)); 
           
        // process the action
        // redirect to the appropriate function 
        switch (strtoupper($action))
        {
            case "CREATE":
                $result = create_rate($rateid,$dbUtils);
                break;

            case "READ":
                $result = read_rate($rateid,$dbUtils);
                break;

            case "UPDATE":
                $result = update_rate($rateid,$dbUtils);
                break;

            case "DELETE":
                $result = delete_rate($rateid,$dbUtils);
                break;

            default: 
                $result = "Unknow action " . $action;
                break;

        }
            
        if (!is_array($result) > 0){
            $result = array($result);
        }

        return $result;
    }
    
    function create_rate($rateid, $dbUtils)
	{

        return "Create not yet implemented";
	}


	function read_rate($rateid,$dbUtils)
	{

        // rates.append(Rate(package: "Memory Mate 8X10 Left", price: 9.95))
        // rates.append(Rate(package: "Memory Mate 8X10 Right", price: 9.95))
        // rates.append(Rate(package: "Memory Mate 5X7 Left", price: 4.95))
        // rates.append(Rate(package: "Team 7X5", price: 11.95))
        // rates.append(Rate(package: "Player 5X7", price: 6.50))

		$msg = "";
		
		// read  the rates table
		$sql = "Select * From vwmpd_rates";
		
		// if a rate id specified, then restrict it by that id
		if($rateid <> "") {
			$sql = $sql . " Where rateid='" . $rateid . "'";
		}
        
        error_log(print_r("rate sql=" . $sql, TRUE)); 

		$rs = $dbUtils->select($sql);

		while ($row = mysqli_fetch_assoc($rs))
		{
			$msg[] = array("id" =>$row['id'],
										"rateid" =>$row['rateid'],
                                        "description" =>$row['description'],
                                        "price" =>$row['price']
										);
		}
		
		if (is_array($msg) > 0){
			$json = $msg;
		}
		else {
			$json = array("No rates found from here...");
		}

		return $json;
		
    }
    
    function update_rate($rateid, $dbUtils)
    {

        return "Update not yet implemented";
    }


    function delete_rate($rateid, $dbUtils)
    {

        return "Delete not yet implemented";
    }



	
?>
