<?php 


	//*************************************************************************************
	//* CRUD routines for orders
	//*************************************************************************************	
    function process_order_action($action, $dbUtils)
    {
        $result = "";

       	// see if a userid was passed
        $userid = get_GET_or_POST('userid');
        // if(isset($_GET['userid'])) {
        //     $userid = $_GET['userid']; //no default
        // }

        $order = get_GET_or_POST('order');
        // if(isset($_GET['order'])) {
        //     $order = $_GET['order']; //no default
        // }

        if ($order != "")
        {
            // process the action
            error_log(print_r("Processing action=" . $action, TRUE)); 

            // redirect to the appropriate function 
            switch (strtoupper($action))
            {
                case "CREATE":
                    $result = create_order($userid,$order,$dbUtils);
                    break;

                case "READ":
                    $result = read_order($userid,$order,$dbUtils);
                    break;

                case "UPDATE":
                    $result = update_order($userid,$order,$dbUtils);
                    break;

                case "DELETE":
                    $result = delete_order($userid,$order,$dbUtils);
                    break;

                default: 
                    $result = '"msg":"Unknow action "' . $action . '"';
                    break;

            }
            
        }
        else 
        {
            $result = '"msg:":"Order not provided"';
        }

        $result = array($result);
        
        return $result;
    }
    
    function create_order($userid, $order, $dbUtils)
	{
        $msg = "";

        // create an order and save it to the database
        $sql = "Insert Into mpd_orders (username, ordersxml) 
                Values('$userid','$order'); ";

        $rs = $dbUtils->insert($sql);

        if ($rs) {				
            $msg = '"msg:":"Order inserted successfully"';
        } 
        else {
            $msg = '"msg:":"Error in insert of order "' . mysql_error() . '" ';
        }			

        return $msg;
	}


	function read_order($userid, $order, $dbUtils)
	{

        $msg = "";
        $json = "Read not yet implemented";
		
		// // read  the users table
		// $sql = "Select * From vwmpd_orders";
		
		// // if a user id specified, then restrict it by that id
		// if($userid <> "") {
		// 	$sql = $sql . " Where username='" . $userid . "'";
		// }
		
		// $rs = $dbUtils->select($sql);

		// while ($row = mysqli_fetch_assoc($rs))
		// {
		// 	$msg[] = array("id" =>$row['ID'],
		// 								"username" =>$row['username'],
		// 								"accountnumber" =>$row['accountnumber'],
		// 								"firstname" =>$row['firstname'], 
		// 								"lastname" =>$row['lastname'],
		// 								"address" =>$row['address'],
		// 								"city" =>$row['city'],
		// 								"state" =>$row['state'],
		// 								"zipcode" =>$row['zipcode'],
		// 								"primaryphone" =>$row['primaryphone'],
		// 								"email" =>$row['email']
		// 								);
		// }
		
		// if (is_array($msg) > 0){
		// 	$json = $msg;
		// }
		// else {
		// 	$json = array("No users found");
		// }

		return $json;
		
    }
    
    function update_order($userid, $order, $dbUtils)
    {

        return '"msg:":"Update not yet implemented"';
    }


    function delete_order($userid, $order, $dbUtils)
    {

        return '"msg:":"Delete not yet implemented"';
    }



	
?>
